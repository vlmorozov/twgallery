<?php

namespace TestWorkBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GalleryControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/gallery/list');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/gallery/view/{id}');
    }

}
