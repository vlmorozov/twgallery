<?php

namespace TestWorkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class GalleryController extends Controller
{
    /**
     * @Route("/gallery/list")
     */
    public function listAction()
    {
        $repositoryGallery = $this->getDoctrine()->getRepository('TestWorkBundle:Gallery');

        $items = $repositoryGallery->findAll();

        $galleries = [];
        if (count($items)) {
            foreach ($items as $item) {
                $galleries[] = [
                    'id' => $item->getId(),
                    'title' => $item->getTitle(),
                ];
            }
        }

        $outdata = [
            'success' => true,
            'data' => $galleries
        ];
        return new JsonResponse($outdata);
    }

    /**
     * @Route("/gallery/view/{galleryId}")
     */
    public function viewAction($galleryId)
    {
        $gallery = $this->getDoctrine()
            ->getRepository('TestWorkBundle:Gallery')
            ->find($galleryId);

        $pictures = $this->getDoctrine()
            ->getRepository('TestWorkBundle:Picture')
            ->findByGallery($gallery);;

        $outdata = [
            'gallery' => $gallery,
            'pictures' => $pictures,
        ];
        return new JsonResponse($outdata);
    }

}
