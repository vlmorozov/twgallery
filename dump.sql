-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.26 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Дамп данных таблицы twgallery.gallery: ~5 rows (приблизительно)
DELETE FROM `gallery`;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` (`id`, `title`) VALUES
	(1, 'Gallery 1'),
	(2, 'Gallery 2'),
	(3, 'Gallery 3'),
	(4, 'Gallery 4'),
	(5, 'Gallery 5');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;

-- Дамп данных таблицы twgallery.picture: ~82 rows (приблизительно)
DELETE FROM `picture`;
/*!40000 ALTER TABLE `picture` DISABLE KEYS */;
INSERT INTO `picture` (`id`, `gallery_id`, `src`, `width`, `height`) VALUES
	(1, 1, '/assets/images/cat1.jpg', 246, 246),
	(2, 1, '/assets/images/cat1.jpg', 246, 246),
	(3, 1, '/assets/images/cat1.jpg', 246, 246),
	(4, 1, '/assets/images/cat1.jpg', 246, 246),
	(5, 1, '/assets/images/cat1.jpg', 246, 246),
	(6, 2, '/assets/images/cat2.jpg', 246, 246),
	(7, 2, '/assets/images/cat2.jpg', 246, 246),
	(8, 2, '/assets/images/cat2.jpg', 246, 246),
	(9, 2, '/assets/images/cat2.jpg', 246, 246),
	(10, 2, '/assets/images/cat2.jpg', 246, 246),
	(11, 2, '/assets/images/cat2.jpg', 246, 246),
	(12, 2, '/assets/images/cat2.jpg', 246, 246),
	(13, 2, '/assets/images/cat2.jpg', 246, 246),
	(14, 2, '/assets/images/cat2.jpg', 246, 246),
	(15, 2, '/assets/images/cat2.jpg', 246, 246),
	(16, 2, '/assets/images/cat2.jpg', 246, 246),
	(17, 2, '/assets/images/cat2.jpg', 246, 246),
	(18, 2, '/assets/images/cat2.jpg', 246, 246),
	(19, 2, '/assets/images/cat2.jpg', 246, 246),
	(20, 2, '/assets/images/cat2.jpg', 246, 246),
	(21, 2, '/assets/images/cat2.jpg', 246, 246),
	(22, 2, '/assets/images/cat2.jpg', 246, 246),
	(23, 2, '/assets/images/cat2.jpg', 246, 246),
	(24, 2, '/assets/images/cat2.jpg', 246, 246),
	(25, 2, '/assets/images/cat2.jpg', 246, 246),
	(26, 2, '/assets/images/cat2.jpg', 246, 246),
	(27, 2, '/assets/images/cat2.jpg', 246, 246),
	(28, 2, '/assets/images/cat2.jpg', 246, 246),
	(29, 2, '/assets/images/cat2.jpg', 246, 246),
	(30, 2, '/assets/images/cat2.jpg', 246, 246),
	(31, 2, '/assets/images/cat2.jpg', 246, 246),
	(32, 2, '/assets/images/cat2.jpg', 246, 246),
	(33, 2, '/assets/images/cat2.jpg', 246, 246),
	(34, 2, '/assets/images/cat2.jpg', 246, 246),
	(35, 2, '/assets/images/cat2.jpg', 246, 246),
	(36, 3, '/assets/images/cat3.jpg', 246, 246),
	(37, 3, '/assets/images/cat3.jpg', 246, 246),
	(38, 3, '/assets/images/cat3.jpg', 246, 246),
	(39, 3, '/assets/images/cat3.jpg', 246, 246),
	(40, 3, '/assets/images/cat3.jpg', 246, 246),
	(41, 3, '/assets/images/cat3.jpg', 246, 246),
	(42, 3, '/assets/images/cat3.jpg', 246, 246),
	(43, 3, '/assets/images/cat3.jpg', 246, 246),
	(44, 3, '/assets/images/cat3.jpg', 246, 246),
	(45, 3, '/assets/images/cat3.jpg', 246, 246),
	(46, 3, '/assets/images/cat3.jpg', 246, 246),
	(47, 3, '/assets/images/cat3.jpg', 246, 246),
	(48, 3, '/assets/images/cat3.jpg', 246, 246),
	(49, 3, '/assets/images/cat3.jpg', 246, 246),
	(50, 3, '/assets/images/cat3.jpg', 246, 246),
	(51, 3, '/assets/images/cat3.jpg', 246, 246),
	(52, 3, '/assets/images/cat3.jpg', 246, 246),
	(53, 3, '/assets/images/cat3.jpg', 246, 246),
	(54, 3, '/assets/images/cat3.jpg', 246, 246),
	(55, 3, '/assets/images/cat3.jpg', 246, 246),
	(56, 3, '/assets/images/cat3.jpg', 246, 246),
	(57, 3, '/assets/images/cat3.jpg', 246, 246),
	(58, 3, '/assets/images/cat3.jpg', 246, 246),
	(59, 3, '/assets/images/cat3.jpg', 246, 246),
	(60, 3, '/assets/images/cat3.jpg', 246, 246),
	(61, 3, '/assets/images/cat3.jpg', 246, 246),
	(62, 3, '/assets/images/cat3.jpg', 246, 246),
	(63, 3, '/assets/images/cat3.jpg', 246, 246),
	(64, 3, '/assets/images/cat3.jpg', 246, 246),
	(65, 3, '/assets/images/cat3.jpg', 246, 246),
	(66, 3, '/assets/images/cat3.jpg', 246, 246),
	(67, 3, '/assets/images/cat3.jpg', 246, 246),
	(68, 3, '/assets/images/cat3.jpg', 246, 246),
	(69, 3, '/assets/images/cat3.jpg', 246, 246),
	(70, 3, '/assets/images/cat3.jpg', 246, 246),
	(71, 3, '/assets/images/cat3.jpg', 246, 246),
	(72, 3, '/assets/images/cat3.jpg', 246, 246),
	(73, 3, '/assets/images/cat3.jpg', 246, 246),
	(74, 3, '/assets/images/cat3.jpg', 246, 246),
	(75, 4, '/assets/images/cat4.jpg', 246, 246),
	(76, 4, '/assets/images/cat4.jpg', 246, 246),
	(77, 4, '/assets/images/cat4.jpg', 246, 246),
	(78, 4, '/assets/images/cat4.jpg', 246, 246),
	(79, 5, '/assets/images/cat1.jpg', 246, 246),
	(80, 5, '/assets/images/cat2.jpg', 246, 246),
	(81, 5, '/assets/images/cat3.jpg', 246, 246),
	(82, 5, '/assets/images/cat4.jpg', 246, 246);
/*!40000 ALTER TABLE `picture` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
